﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public enum Direction { Left, Right, Up, Down};

    class Settings
    {
        public static int SquareWidth { get; set; }
        public static int SquareHeight { get; set; }
        public static int Speed { get; set; }
        public static int PointsSum { get; set; }
        public static int Points { get; set; }
        public static int LinesAmount { get; set; }
        public static bool GameOver { get; set; }
        public static Direction Direction { get; set; }
        public static int Level { get; set; }

        public Settings()
        {
            SquareWidth = 20;
            SquareHeight = 20;
            Speed = 1;
            PointsSum = 0;
            Points = 10;
            LinesAmount = 0;
            GameOver = false;
            Direction = Direction.Down;
            Level = 1;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tetris
{
    public partial class Form1 : Form
    {
        Label[] labelsArray = new Label[200];
        Label[] labelArray2 = new Label[16];

        int labelNumber = 0;
        int labelRow = 0;
        int labelNumberInRow = 0;

        int label2Number = 0;
        int label2Row = 0;
        int label2NumberInRow = 0;

        Figure presentFigure;
        Figure nextFigure;


        public Form1()
        {
            InitializeComponent();

            NewGame();
            panel1.Size = new Size(10 * 20 + 4 + 20, 20 * 20 + 4 + 40);  //10 w poziomie bo 10 klocków, 20 w pionie, 4 to suma odstępu klocków od scian po 2 piksele z lewej i prawej
            panel2.Size = new Size(4 * 20 + 4 + 8, 4 * 20 + 4 + 8);
            label2.Text = Settings.Level.ToString();
            label4.Text = Settings.PointsSum.ToString();
            label6.Text = Settings.LinesAmount.ToString();

            foreach (Label label in labelsArray)
            {
                labelsArray[labelNumber] = new Label();
                labelsArray[labelNumber].Parent = panel1;
                labelsArray[labelNumber].Size = new Size(20, 20);
                labelsArray[labelNumber].Location = new Point(2 + 22 * labelNumberInRow, 2 + 22 * labelRow);
                labelsArray[labelNumber].Name = "label" + labelNumber.ToString();
                labelsArray[labelNumber].BackColor = Color.Green;

                labelNumber++;
                labelNumberInRow++;
                if (labelNumberInRow == 10)
                {
                    labelNumberInRow = 0;
                    labelRow++;
                }
            }

            foreach (Label label in labelArray2)
            {
                labelArray2[label2Number] = new Label();
                labelArray2[label2Number].Parent = panel2;
                labelArray2[label2Number].Size = new Size(20, 20);
                labelArray2[label2Number].Location = new Point(2 + 22 * label2NumberInRow, 2 + 22 * label2Row);
                labelArray2[label2Number].Name = "labelNext" + label2Number.ToString();
                labelArray2[label2Number].BackColor = Color.Green;

                label2Number++;
                label2NumberInRow++;
                if (label2NumberInRow == 4)
                {
                    label2NumberInRow = 0;
                    label2Row++;
                }
            }

            System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
            timer1.Interval = 1000 / Settings.Speed;
            timer1.Tick += Timer1_Tick;
            timer1.Start();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            label2.Text = Settings.Level.ToString();
            label4.Text = Settings.PointsSum.ToString();
            label6.Text = Settings.LinesAmount.ToString();
            if (!Settings.GameOver)
            {
                if (presentFigure == null || presentFigure.Squares == null)
                {
                    presentFigure = ChooseFigure();
                }
                if (nextFigure == null || nextFigure.Squares == null)
                {
                    nextFigure = ChooseFigure();
                    while (nextFigure.Type == presentFigure.Type)
                    {
                        nextFigure = ChooseFigure();
                    }
                    ClearShowNextFigureArray();
                    ShowNextFigure();
                }

                int counter = 1;
                bool moveFigure = true;
                foreach (Square square in presentFigure.Squares)
                {
                    if (counter == 1 && square.Position < 0)
                    {
                        if (labelsArray[5].BackColor == Color.Green)
                        {
                            square.NextPosition = 5;
                        }
                        else
                        {
                            EndGame();
                        }
                    }
                    else if (counter == 1)
                    {
                        int squareNextPosition = square.Position + 10;
                        if (squareNextPosition > 199)
                        {
                            moveFigure = false;
                            presentFigure = null;
                            presentFigure = nextFigure;
                            nextFigure = null;
                            nextFigure = ChooseFigure();
                            ClearShowNextFigureArray();
                            ShowNextFigure();
                            break;
                        }
                        else if (labelsArray[squareNextPosition].BackColor == Color.Green)
                        {
                            square.NextPosition = squareNextPosition;
                        }
                        else
                        {
                            moveFigure = false;
                            presentFigure = null;
                            presentFigure = nextFigure;
                            nextFigure = null;
                            nextFigure = ChooseFigure();
                            ClearShowNextFigureArray();
                            ShowNextFigure();
                            break;
                        }
                    }
                    else
                    {
                        int squareNextPosition = presentFigure.Squares[0].NextPosition + (square.X) - square.Y;
                        if (squareNextPosition < 0)
                        {
                            continue;
                        }
                        else if (squareNextPosition > 199)
                        {
                            moveFigure = false;
                            presentFigure = null;
                            presentFigure = nextFigure;
                            nextFigure = null;
                            nextFigure = ChooseFigure();
                            ClearShowNextFigureArray();
                            ShowNextFigure();
                            break;
                        }
                        else if (square.DownFlag && labelsArray[squareNextPosition].BackColor == Color.Blue)
                        {
                            moveFigure = false;
                            presentFigure = null;
                            presentFigure = nextFigure;
                            nextFigure = null;
                            nextFigure = ChooseFigure();
                            ClearShowNextFigureArray();
                            ShowNextFigure();
                            break;
                        }
                        else
                        {
                            square.NextPosition = squareNextPosition;
                        }
                    }
                    counter++;
                }
                if (moveFigure)
                {
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.Position >= 0)
                        {
                            labelsArray[square.Position].BackColor = Color.Green;
                        }
                        if (square.NextPosition >= 0)
                        {
                            labelsArray[square.NextPosition].BackColor = Color.Blue;
                        }
                        square.Position = square.NextPosition;
                    }
                }
                else
                {
                    CheckLines();
                }
            }
        }

        private void CheckLines()
        {
            int linesDownCounter = 0;
            for (int i = 19; i >= 0; i--)
            {
                bool lineBlue = true;
                for (int j = 9; j >= 0; j--)
                {
                    int labelIndex = j + (i * 10);
                    if (labelsArray[labelIndex].BackColor == Color.Green)
                    {
                        lineBlue = false;
                    }
                    if (linesDownCounter > 0)
                    {
                        labelsArray[labelIndex + linesDownCounter * 10].BackColor = labelsArray[labelIndex].BackColor;
                        if (labelIndex <= 9)
                        {
                            labelsArray[labelIndex].BackColor = Color.Green;
                        }
                    }
                }
                if (lineBlue)
                {
                    linesDownCounter++;
                    Settings.LinesAmount += 1;

                    for (int j = 0; j < 10; j++)
                    {
                        int labelIndex = j + (i * 10);
                        Thread.Sleep(100);
                        labelsArray[labelIndex].BackColor = Color.Green;
                    }
                }

            }
            Settings.PointsSum += Settings.Points * linesDownCounter * linesDownCounter;

        }

        private void NewGame()
        {
            foreach (Label label in labelsArray)
            {
                if (label != null)
                {
                    label.BackColor = Color.Green;
                }
            }
            panel3.Visible = false;
            presentFigure = null;
            nextFigure = null;
            new Settings();
        }

        private void EndGame()
        {
            panel3.Visible = true;
            label8.Text = "Koniec gry! Twój wynik to " + Settings.PointsSum + " punktów.";
            Settings.GameOver = true;
        }

        private Figure ChooseFigure()
        {
            Figures figures = new Figures();
            Random random = new Random();
            int figureID = random.Next(1, figures.List.Count + 1);
            return figures.List.Where(x => x.ID == figureID).FirstOrDefault();
        }

        private void ShowNextFigure()
        {
            int[] blueSquares;
            if (nextFigure.ID == 1)
            {
                blueSquares = new int[] { 2, 6, 10, 14 };
            }
            else if (nextFigure.ID == 2)
            {
                blueSquares = new int[] { 8, 9, 10, 11 };
            }
            else if (nextFigure.ID == 3)
            {
                blueSquares = new int[] { 5, 6, 9, 10 };
            }
            else if (nextFigure.ID == 4)
            {
                blueSquares = new int[] { 5, 6, 10, 11 };
            }
            else if (nextFigure.ID == 5)
            {
                blueSquares = new int[] { 6, 9, 10, 13 };
            }
            else if (nextFigure.ID == 6)
            {
                blueSquares = new int[] { 5, 6, 8, 9 };
            }
            else if (nextFigure.ID == 7)
            {
                blueSquares = new int[] { 5, 9, 10, 14 };
            }
            else if (nextFigure.ID == 8)
            {
                blueSquares = new int[] { 2, 6, 9, 10 };
            }
            else if (nextFigure.ID == 9)
            {
                blueSquares = new int[] { 5, 9, 10, 11 };
            }
            else if (nextFigure.ID == 10)
            {
                blueSquares = new int[] { 5, 6, 9, 13 };
            }
            else if (nextFigure.ID == 11)
            {
                blueSquares = new int[] { 4, 5, 6, 10 };
            }
            else if (nextFigure.ID == 12)
            {
                blueSquares = new int[] { 1, 5, 9, 10 };
            }
            else if (nextFigure.ID == 13)
            {
                blueSquares = new int[] { 5, 6, 7, 9 };
            }
            else if (nextFigure.ID == 14)
            {
                blueSquares = new int[] { 5, 6, 10, 14 };
            }
            else if (nextFigure.ID == 15)
            {
                blueSquares = new int[] { 6, 8, 9, 10 };
            }
            else if (nextFigure.ID == 16)
            {
                blueSquares = new int[] { 2, 5, 6, 10 };
            }
            else if (nextFigure.ID == 17)
            {
                blueSquares = new int[] { 6, 9, 10, 11 };
            }
            else if (nextFigure.ID == 18)
            {
                blueSquares = new int[] { 1, 5, 6, 9 };
            }
            else if (nextFigure.ID == 19)
            {
                blueSquares = new int[] { 5, 6, 7, 10 };
            }
            else
            {
                blueSquares = new int[] { 0, 3, 12, 15 };
            }

            foreach (int squareIndex in blueSquares)
            {
                labelArray2[squareIndex].BackColor = Color.Blue;
            }
        }

        private void ClearShowNextFigureArray()
        {
            foreach (Label label in labelArray2)
            {
                if (label.BackColor == Color.Blue)
                {
                    label.BackColor = Color.Green;
                }
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Down && !Settings.GameOver)
            {
                if (!Settings.GameOver)
                {
                    if (presentFigure == null || presentFigure.Squares == null)
                    {
                        presentFigure = ChooseFigure();
                    }
                    if (nextFigure == null || nextFigure.Squares == null)
                    {
                        nextFigure = ChooseFigure();
                        while (nextFigure.Type == presentFigure.Type)
                        {
                            nextFigure = ChooseFigure();
                        }
                        ClearShowNextFigureArray();
                        ShowNextFigure();
                    }

                    int counter = 1;
                    bool moveFigure = true;
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (counter == 1 && square.Position < 0)
                        {
                            if (labelsArray[5].BackColor == Color.Green)
                            {
                                square.NextPosition = 5;
                            }
                            else
                            {
                                EndGame();
                            }
                        }
                        else if (counter == 1)
                        {
                            int squareNextPosition = square.Position + 10;
                            if (squareNextPosition > 199)
                            {
                                moveFigure = false;
                                presentFigure = null;
                                presentFigure = nextFigure;
                                nextFigure = null;
                                nextFigure = ChooseFigure();
                                ClearShowNextFigureArray();
                                ShowNextFigure();
                                break;
                            }
                            else if (labelsArray[squareNextPosition].BackColor == Color.Green)
                            {
                                square.NextPosition = squareNextPosition;
                            }
                            else
                            {
                                moveFigure = false;
                                presentFigure = null;
                                presentFigure = nextFigure;
                                nextFigure = null;
                                nextFigure = ChooseFigure();
                                ClearShowNextFigureArray();
                                ShowNextFigure();
                                break;
                            }
                        }
                        else
                        {
                            int squareNextPosition = presentFigure.Squares[0].NextPosition + (square.X) - square.Y;
                            if (squareNextPosition < 0)
                            {
                                continue;
                            }
                            else if (squareNextPosition > 199)
                            {
                                moveFigure = false;
                                presentFigure = null;
                                presentFigure = nextFigure;
                                nextFigure = null;
                                nextFigure = ChooseFigure();
                                ClearShowNextFigureArray();
                                ShowNextFigure();
                                break;
                            }
                            else if (square.DownFlag && labelsArray[squareNextPosition].BackColor == Color.Blue)
                            {
                                moveFigure = false;
                                presentFigure = null;
                                presentFigure = nextFigure;
                                nextFigure = null;
                                nextFigure = ChooseFigure();
                                ClearShowNextFigureArray();
                                ShowNextFigure();
                                break;
                            }
                            else
                            {
                                square.NextPosition = squareNextPosition;
                            }
                        }
                        counter++;
                    }
                    if (moveFigure)
                    {
                        foreach (Square square in presentFigure.Squares)
                        {
                            if (square.Position >= 0)
                            {
                                labelsArray[square.Position].BackColor = Color.Green;
                            }
                            if (square.NextPosition >= 0)
                            {
                                labelsArray[square.NextPosition].BackColor = Color.Blue;
                            }
                            square.Position = square.NextPosition;
                        }
                    }
                    else
                    {
                        CheckLines();
                    }
                }
            }
            if (keyData == Keys.Left && !Settings.GameOver)
            {
                bool moveFigure = true;
                foreach (Square square in presentFigure.Squares)
                {
                    if (presentFigure.Squares[0].NextPosition.ToString().Last() == '0' && square.X < 0 && square.Y > 0)
                    {
                        moveFigure = false;
                        break;
                    }
                    else if (square.Position < 0)
                    {
                        continue;
                    }

                    int squareNextPosition = square.Position - 1;
                    if (square.Position % 10 == 0)
                    {
                        moveFigure = false;
                        break;
                    }
                    else if (square.LeftFlag && squareNextPosition >= 0 && labelsArray[squareNextPosition].BackColor == Color.Blue)
                    {
                        moveFigure = false;
                        break;
                    }
                    else
                    {
                        square.NextPosition = squareNextPosition;
                    }
                }
                if (moveFigure)
                {
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.Position >= 0)
                        {
                            labelsArray[square.Position].BackColor = Color.Green;
                        }
                    }
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.NextPosition >= 0)
                        {
                            labelsArray[square.NextPosition].BackColor = Color.Blue;
                        }
                        square.Position = square.NextPosition;
                    }
                }
            }
            if (keyData == Keys.Right && !Settings.GameOver)
            {
                bool moveFigure = true;
                foreach (Square square in presentFigure.Squares)
                {
                    if (presentFigure.Squares[0].NextPosition.ToString().Last() == '9' && square.X > 0 && square.Y > 0)
                    {
                        moveFigure = false;
                        break;
                    }
                    else if (square.Position < 0)
                    {
                        continue;
                    }

                    int squareNextPosition = square.Position + 1;
                    if (square.Position.ToString().Last() == '9')
                    {
                        moveFigure = false;
                        break;
                    }
                    else if (square.RightFlag && squareNextPosition >= 0 && labelsArray[squareNextPosition].BackColor == Color.Blue)
                    {
                        moveFigure = false;
                        break;
                    }
                    else
                    {
                        square.NextPosition = squareNextPosition;
                    }
                }
                if (moveFigure)
                {
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.Position >= 0)
                        {
                            labelsArray[square.Position].BackColor = Color.Green;
                        }
                    }
                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.NextPosition >= 0)
                        {
                            labelsArray[square.NextPosition].BackColor = Color.Blue;
                        }
                        square.Position = square.NextPosition;
                    }
                }
            }
            if (keyData == Keys.Space && !Settings.GameOver)
            {
                Figures figures = new Figures();
                int figureCombinationsAmount = figures.List.Count(x => x.Type == presentFigure.Type);

                int nextTypeID = presentFigure.TypeID + 1;
                if (nextTypeID > figureCombinationsAmount)
                {
                    nextTypeID = 1;
                }

                Figure newFigure = figures.List.FirstOrDefault(x => x.Type == presentFigure.Type && x.TypeID == nextTypeID);
                int presentFigureTurnSquarePosition = presentFigure.Squares[presentFigure.TurnSquareIndex].Position;
                if (presentFigureTurnSquarePosition >= 0 && !(presentFigureTurnSquarePosition % 10 == 0) && presentFigureTurnSquarePosition.ToString().Last() != '9')
                {
                    int newFigureFirstSquarePosition = presentFigureTurnSquarePosition + newFigure.Squares[newFigure.TurnSquareIndex].Y - (newFigure.Squares[newFigure.TurnSquareIndex].X);
                    bool switchFigure = true;

                    foreach (Square square in presentFigure.Squares)
                    {
                        if (square.Position >= 0)
                        {
                            labelsArray[square.Position].BackColor = Color.Green;
                        }
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        if (i == 0)
                        {
                            newFigure.Squares[i].Position = newFigure.Squares[i].NextPosition = newFigureFirstSquarePosition;
                        }
                        else
                        {
                            newFigure.Squares[i].Position = newFigure.Squares[i].NextPosition = newFigure.Squares[0].Position + (newFigure.Squares[i].X) - newFigure.Squares[i].Y;
                        }
                        if (newFigure.Squares[i].Position >= 0 && labelsArray[newFigure.Squares[i].Position].BackColor == Color.Blue)
                        {
                            switchFigure = false;
                            break;
                        }
                    }
                    if (switchFigure)
                    {
                        foreach (Square square in newFigure.Squares)
                        {
                            if (square.Position >= 0)
                            {
                                labelsArray[square.Position].BackColor = Color.Blue;
                            }
                        }
                        presentFigure = newFigure;
                    }
                    else
                    {
                        foreach (Square square in presentFigure.Squares)
                        {
                            if (square.Position >= 0)
                            {
                                labelsArray[square.Position].BackColor = Color.Blue;
                            }
                        }
                    }
                }
            }
            if (keyData == Keys.Enter && Settings.GameOver)
            {
                NewGame();
            }
            if (keyData == Keys.Escape && Settings.GameOver)
            {
                this.Close();
            }

            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NewGame();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    public enum EFigures { Linia, Blok, EsLewe, EsPrawe, ElLewe, ElPrawe, Te};

    class Figure
    {
        public int ID { get; set; }
        public EFigures Type { get; set; }
        public int TypeID { get; set; }
        public bool Shown { get; set; }
        public List<Square> Squares { get; set; }
        public int TurnSquareIndex { get; set; }
    }
}

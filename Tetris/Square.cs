﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Square
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Position { get; set; }
        public int NextPosition { get; set; }
        public bool LeftFlag { get; set; }
        public bool RightFlag { get; set; }
        public bool DownFlag { get; set; }

        public Square(int x, int y, bool leftFlag, bool rightFlag, bool downFlag)
        {
            X = x;
            Y = y;
            Position = -10;
            NextPosition = -10;
            LeftFlag = leftFlag;
            RightFlag = rightFlag;
            DownFlag = downFlag;
        }
    }
}

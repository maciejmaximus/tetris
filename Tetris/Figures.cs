﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tetris
{
    class Figures
    {
        public List<Figure> List { get; set; }


        public Figures()
        {
            List = new List<Figure>();
            List.Add(new Figure() { ID = 1, Type = EFigures.Linia, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, true, true, false), new Square(0, 20, true, true, false), new Square(0, 30, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 2, Type = EFigures.Linia, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, false, false, true), new Square(-2, 0, false, false, true), new Square(-3, 0, true, false, true) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 3, Type = EFigures.Blok, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, true, false, true), new Square(0, 10, false, true, false) , new Square(-1, 10, true, false, false) }, TurnSquareIndex = 0 });
            List.Add(new Figure() { ID = 4, Type = EFigures.EsLewe, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, false, true), new Square(0, 10, false, true, false), new Square(-1, 10, true, false, true), new Square(1, 0, false, true, true) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 5, Type = EFigures.EsLewe, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(1, 10, false, true, true), new Square(0, 10, true, false, false), new Square(1, 20, true, true, false) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 6, Type = EFigures.EsPrawe, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(0, 10, true, false, false), new Square(1, 10, false, true, true), new Square(-1, 0, true, false, true) }, TurnSquareIndex = 0 });
            List.Add(new Figure() { ID = 7, Type = EFigures.EsPrawe, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, false, true, false), new Square(-1, 10, true, false, true), new Square(-1, 20, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 8, Type = EFigures.ElLewe, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, true, false, true), new Square(0, 10, true, true, false), new Square(0, 20, true, true, false) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 9, Type = EFigures.ElLewe, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, false, false, true), new Square(-2, 0, true, false, true), new Square(-2, 10, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 10, Type = EFigures.ElLewe, TypeID = 3, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, true, true, false), new Square(1, 20, false, true, true), new Square(0, 20, true, false, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 11, Type = EFigures.ElLewe, TypeID = 4, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, false, true, false), new Square(-1, 10, false, false, true), new Square(-2, 10, true, false, true) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 12, Type = EFigures.ElPrawe, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, false, true), new Square(1, 0, false, true, true), new Square(0, 10, true, true, false), new Square(0, 20, true, true, false) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 13, Type = EFigures.ElPrawe, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(2, 10, false, true, true), new Square(1, 10, false, false, true), new Square(0, 10, true, false, false) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 14, Type = EFigures.ElPrawe, TypeID = 3, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, true, true, false), new Square(0, 20, false, true, false), new Square(-1, 20, true, false, true) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 15, Type = EFigures.ElPrawe, TypeID = 4, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, false, false, true), new Square(-2, 0, true, false, true), new Square(0, 10, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 16, Type = EFigures.Te, TypeID = 1, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(0, 10, false, true, false), new Square(-1, 10, true, false, true), new Square(0, 20, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 17, Type = EFigures.Te, TypeID = 2, Shown = false, Squares = new List<Square>() { new Square(0, 0, false, true, true), new Square(-1, 0, false, false, true), new Square(-2, 0, true, false, true), new Square(-1, 10, true, true, false) }, TurnSquareIndex = 1 });
            List.Add(new Figure() { ID = 18, Type = EFigures.Te, TypeID = 3, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(1, 10, false, true, true), new Square(0, 10, true, false, false), new Square(0, 20, true, true, false) }, TurnSquareIndex = 2 });
            List.Add(new Figure() { ID = 19, Type = EFigures.Te, TypeID = 4, Shown = false, Squares = new List<Square>() { new Square(0, 0, true, true, true), new Square(1, 10, false, true, true), new Square(0, 10, false, false, false), new Square(-1, 10, true, false, true) }, TurnSquareIndex = 2 });
        }
    }
}
